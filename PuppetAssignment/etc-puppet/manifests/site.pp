node default {
	
	$servers = [
		{
			ip_address => '127.0.0.1',
			hostname => 'site1.puppet.sriram.in'
		},
		{
			ip_address => '127.0.0.1',
			hostname => 'site2.puppet.sriram.in'
		}
	
	]
	nginx::vhost::apply{'website1':
		domain => 'site1.puppet.sriram.in',
        root => '/home/ubuntu/site1'
	}
	nginx::vhost::apply{'website2':
		domain => 'site2.puppet.sriram.in',
        root => '/home/ubuntu/site2'
	}
	class { 'nginx::hosts' :
        servername => $servers
	}
}